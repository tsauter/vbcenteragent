package main

import (
	proto "gitlab.com/tsauter/vbcenteragent/proto"
)

type VirtualBoxAPI interface {
	LoadEnvironment() error
	GetHostname() string
	GetMachines() []*proto.Machine
}
