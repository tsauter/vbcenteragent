package main

import (
	"log"
)

type Logger interface {
	Close() error
	Error(eid uint32, msg string) error
	Info(eid uint32, msg string) error
	Warning(eid uint32, msg string) error
}

type ConsoleLogger struct {
}

func (ul *ConsoleLogger) Close() error {
	return nil
}

func (ul *ConsoleLogger) Error(eid uint32, msg string) error {
	log.Printf("%s\n", msg)
	return nil
}

func (ul *ConsoleLogger) Info(eid uint32, msg string) error {
	log.Printf("%s\n", msg)
	return nil
}

func (ul *ConsoleLogger) Warning(eid uint32, msg string) error {
	log.Printf("%s\n", msg)
	return nil
}
