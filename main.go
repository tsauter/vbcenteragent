// +build windows

package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/spf13/viper"
	"golang.org/x/sys/windows/svc"
)

const svcName = "virtualboxcenterhostagent"
const svcLongName = "VirtualBox Center Host Agent"

var (
	vbox VirtualBoxAPI
)

func usage(errmsg string) {
	fmt.Fprintf(os.Stderr,
		"%s\n\n"+
			"usage: %s <command>\n"+
			"       where <command> is one of\n"+
			"       version, install, remove, debug, start, stop, pause or continue.\n",
		errmsg, svcName)
	os.Exit(2)
}

func version() {
	fmt.Printf("%s", svcName)
	//fmt.Printf("%s v%s [%s] (compiled on %s at %s)", svcName, core.Version, core.Branch, core.BuildUser, core.BuildDate)
}

func main() {
	viper.SetDefault("Hostname", "localhost")
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %v", err))
	}

	isIntSess, err := svc.IsAnInteractiveSession()
	if err != nil {
		log.Fatalf("failed to determine if we are running in an interactive session: %v", err)
	}
	if !isIntSess {
		runService(svcName, false)
		return
	}

	if len(os.Args) < 2 {
		usage("no command specified")
	}

	cmd := strings.ToLower(os.Args[1])
	switch cmd {
	case "version":
		version()
		return
	case "debug":
		runService(svcName, true)
		return
	case "install":
		err = installService(svcName, svcLongName)
	case "remove":
		err = removeService(svcName)
	case "start":
		err = startService(svcName)
	case "stop":
		err = controlService(svcName, svc.Stop, svc.Stopped)
	case "pause":
		err = controlService(svcName, svc.Pause, svc.Paused)
	case "continue":
		err = controlService(svcName, svc.Continue, svc.Running)
	default:
		usage(fmt.Sprintf("invalid command %s", cmd))
	}
	if err != nil {
		log.Fatalf("failed to %s %s: %v", cmd, svcName, err)
	}

	return
}
