// Code generated by protoc-gen-go. DO NOT EDIT.
// source: virtualbox.proto

package virtualbox

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type GetMachinesRequest struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetMachinesRequest) Reset()         { *m = GetMachinesRequest{} }
func (m *GetMachinesRequest) String() string { return proto.CompactTextString(m) }
func (*GetMachinesRequest) ProtoMessage()    {}
func (*GetMachinesRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_virtualbox_7c6c5518e2c2993c, []int{0}
}
func (m *GetMachinesRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetMachinesRequest.Unmarshal(m, b)
}
func (m *GetMachinesRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetMachinesRequest.Marshal(b, m, deterministic)
}
func (dst *GetMachinesRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetMachinesRequest.Merge(dst, src)
}
func (m *GetMachinesRequest) XXX_Size() int {
	return xxx_messageInfo_GetMachinesRequest.Size(m)
}
func (m *GetMachinesRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetMachinesRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetMachinesRequest proto.InternalMessageInfo

type Machine struct {
	ID                   string   `protobuf:"bytes,1,opt,name=ID,proto3" json:"ID,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=Name,proto3" json:"Name,omitempty"`
	Host                 string   `protobuf:"bytes,3,opt,name=Host,proto3" json:"Host,omitempty"`
	RunState             int32    `protobuf:"varint,4,opt,name=RunState,proto3" json:"RunState,omitempty"`
	RDPPort              int64    `protobuf:"varint,5,opt,name=RDPPort,proto3" json:"RDPPort,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Machine) Reset()         { *m = Machine{} }
func (m *Machine) String() string { return proto.CompactTextString(m) }
func (*Machine) ProtoMessage()    {}
func (*Machine) Descriptor() ([]byte, []int) {
	return fileDescriptor_virtualbox_7c6c5518e2c2993c, []int{1}
}
func (m *Machine) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Machine.Unmarshal(m, b)
}
func (m *Machine) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Machine.Marshal(b, m, deterministic)
}
func (dst *Machine) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Machine.Merge(dst, src)
}
func (m *Machine) XXX_Size() int {
	return xxx_messageInfo_Machine.Size(m)
}
func (m *Machine) XXX_DiscardUnknown() {
	xxx_messageInfo_Machine.DiscardUnknown(m)
}

var xxx_messageInfo_Machine proto.InternalMessageInfo

func (m *Machine) GetID() string {
	if m != nil {
		return m.ID
	}
	return ""
}

func (m *Machine) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Machine) GetHost() string {
	if m != nil {
		return m.Host
	}
	return ""
}

func (m *Machine) GetRunState() int32 {
	if m != nil {
		return m.RunState
	}
	return 0
}

func (m *Machine) GetRDPPort() int64 {
	if m != nil {
		return m.RDPPort
	}
	return 0
}

type GetMachinesResponse struct {
	VM                   []*Machine `protobuf:"bytes,1,rep,name=VM,proto3" json:"VM,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *GetMachinesResponse) Reset()         { *m = GetMachinesResponse{} }
func (m *GetMachinesResponse) String() string { return proto.CompactTextString(m) }
func (*GetMachinesResponse) ProtoMessage()    {}
func (*GetMachinesResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_virtualbox_7c6c5518e2c2993c, []int{2}
}
func (m *GetMachinesResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetMachinesResponse.Unmarshal(m, b)
}
func (m *GetMachinesResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetMachinesResponse.Marshal(b, m, deterministic)
}
func (dst *GetMachinesResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetMachinesResponse.Merge(dst, src)
}
func (m *GetMachinesResponse) XXX_Size() int {
	return xxx_messageInfo_GetMachinesResponse.Size(m)
}
func (m *GetMachinesResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetMachinesResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetMachinesResponse proto.InternalMessageInfo

func (m *GetMachinesResponse) GetVM() []*Machine {
	if m != nil {
		return m.VM
	}
	return nil
}

func init() {
	proto.RegisterType((*GetMachinesRequest)(nil), "GetMachinesRequest")
	proto.RegisterType((*Machine)(nil), "Machine")
	proto.RegisterType((*GetMachinesResponse)(nil), "GetMachinesResponse")
}

func init() { proto.RegisterFile("virtualbox.proto", fileDescriptor_virtualbox_7c6c5518e2c2993c) }

var fileDescriptor_virtualbox_7c6c5518e2c2993c = []byte{
	// 225 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x28, 0xcb, 0x2c, 0x2a,
	0x29, 0x4d, 0xcc, 0x49, 0xca, 0xaf, 0xd0, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x57, 0x12, 0xe1, 0x12,
	0x72, 0x4f, 0x2d, 0xf1, 0x4d, 0x4c, 0xce, 0xc8, 0xcc, 0x4b, 0x2d, 0x0e, 0x4a, 0x2d, 0x2c, 0x4d,
	0x2d, 0x2e, 0x51, 0x2a, 0xe7, 0x62, 0x87, 0x0a, 0x09, 0xf1, 0x71, 0x31, 0x79, 0xba, 0x48, 0x30,
	0x2a, 0x30, 0x6a, 0x70, 0x06, 0x31, 0x79, 0xba, 0x08, 0x09, 0x71, 0xb1, 0xf8, 0x25, 0xe6, 0xa6,
	0x4a, 0x30, 0x81, 0x45, 0xc0, 0x6c, 0x90, 0x98, 0x47, 0x7e, 0x71, 0x89, 0x04, 0x33, 0x44, 0x0c,
	0xc4, 0x16, 0x92, 0xe2, 0xe2, 0x08, 0x2a, 0xcd, 0x0b, 0x2e, 0x49, 0x2c, 0x49, 0x95, 0x60, 0x51,
	0x60, 0xd4, 0x60, 0x0d, 0x82, 0xf3, 0x85, 0x24, 0xb8, 0xd8, 0x83, 0x5c, 0x02, 0x02, 0xf2, 0x8b,
	0x4a, 0x24, 0x58, 0x15, 0x18, 0x35, 0x98, 0x83, 0x60, 0x5c, 0x25, 0x7d, 0x2e, 0x61, 0x14, 0xe7,
	0x14, 0x17, 0xe4, 0xe7, 0x15, 0x83, 0x34, 0x30, 0x85, 0xf9, 0x4a, 0x30, 0x2a, 0x30, 0x6b, 0x70,
	0x1b, 0x71, 0xe8, 0x41, 0xa5, 0x83, 0x98, 0xc2, 0x7c, 0x8d, 0x02, 0xb9, 0x84, 0xc3, 0x20, 0x7e,
	0x72, 0xca, 0xaf, 0x00, 0x59, 0xec, 0x98, 0x9e, 0x9a, 0x57, 0x22, 0x64, 0xc5, 0xc5, 0x8d, 0x64,
	0x8e, 0x90, 0xb0, 0x1e, 0xa6, 0x27, 0xa5, 0x44, 0xf4, 0xb0, 0x58, 0xa5, 0xc4, 0x90, 0xc4, 0x06,
	0x0e, 0x19, 0x63, 0x40, 0x00, 0x00, 0x00, 0xff, 0xff, 0xd9, 0xa9, 0x0b, 0xda, 0x2d, 0x01, 0x00,
	0x00,
}
