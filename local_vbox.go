package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os/exec"
	"regexp"
	"strconv"
	"sync"

	proto "gitlab.com/tsauter/vbcenteragent/proto"

	"github.com/pkg/errors"
)

type LocalVirtualBox struct {
	m        sync.RWMutex
	Hostname string
	Machines []*proto.Machine
}

func NewLocalVirtualBox(hostname string) (*LocalVirtualBox, error) {
	lvb := LocalVirtualBox{Hostname: hostname}
	return &lvb, nil
}

func (lvb *LocalVirtualBox) LoadEnvironment() error {
	fmt.Printf("Reloading environment...\n")

	machines, err := lvb.loadMachines()
	if err != nil {
		fmt.Printf("Reloading failed: %s\n", err)
		//return errors.Wrap(err, "failed to load virtualbox environment")
		return nil
	}

	lvb.m.Lock()
	defer lvb.m.Unlock()
	lvb.Machines = machines

	return nil
}

func (lvb *LocalVirtualBox) loadMachines() ([]*proto.Machine, error) {
	cmd := exec.Command(`C:\Program Files\Oracle\VirtualBox\VBoxManage.exe`, "list", "vms")
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		return nil, errors.Wrap(err, "failed to load VMs")
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	//fmt.Printf("out:\n%s\nerr:\n%s\n", outStr, errStr)

	machines := []*proto.Machine{}

	regexListVMS := regexp.MustCompile(`"(.*)"\s+{(.*)}`)

	scanner := bufio.NewScanner(&stdout)
	for scanner.Scan() {
		line := scanner.Text()

		rgroups := regexListVMS.FindStringSubmatch(line)
		if len(rgroups) != 3 {
			return nil, fmt.Errorf("invalid VM line: %s", line)
		}

		id := rgroups[2]
		m, err := lvb.loadMachineSettings(id)
		if err != nil {
			return nil, errors.Wrap(err, "faild to load VM properties")
		}
		m.Name = rgroups[1]
		fmt.Printf("%v\n", m)
		machines = append(machines, m)
	}

	return machines, nil
}

func (lvb *LocalVirtualBox) loadMachineSettings(id string) (*proto.Machine, error) {
	cmd := exec.Command(`C:\Program Files\Oracle\VirtualBox\VBoxManage.exe`, "showvminfo", id)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		return nil, errors.Wrap(err, "failed to load VMs")
	}
	//outStr, errStr := string(stdout.Bytes()), string(stderr.Bytes())
	//fmt.Printf("out:\n%s\nerr:\n%s\n", outStr, errStr)

	regexState := regexp.MustCompile(`^State:\s+(.*)\s\(`)
	regexVRDPPort := regexp.MustCompile(`^VRDE property: TCP/Ports  = "(\d+)"$`)

	m := proto.Machine{ID: id, Host: lvb.GetHostname()}

	scanner := bufio.NewScanner(&stdout)
	for scanner.Scan() {
		line := scanner.Text()

		rgroups := regexState.FindStringSubmatch(line)
		if len(rgroups) == 2 {
			switch rgroups[1] {
			case "powered off":
				m.RunState = 2
				break
			case "running":
				m.RunState = 1
				break
			case "paused":
				m.RunState = 2
				break
			case "aborted":
				m.RunState = 3
				break
			default:
				panic(fmt.Errorf("Invalid machine state: %s: %s\n", id, rgroups[1]))
				m.RunState = -1
				break
			}
		}

		rgroups = regexVRDPPort.FindStringSubmatch(line)
		if len(rgroups) == 2 {
			i, err := strconv.ParseInt(rgroups[1], 10, 0)
			if err == nil {
				//fmt.Printf("%s\n", line)
				m.RDPPort = i
			}
		}
	}

	return &m, nil
}

func (lvb *LocalVirtualBox) GetHostname() string {
	return lvb.Hostname
}

func (lvb *LocalVirtualBox) GetMachines() []*proto.Machine {
	lvb.m.RLock()
	defer lvb.m.RUnlock()
	return lvb.Machines
}
