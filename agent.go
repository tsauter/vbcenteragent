package main

import (
	"context"
	"fmt"
	"time"

	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/registry/consul"
	"github.com/spf13/viper"

	proto "gitlab.com/tsauter/vbcenteragent/proto"
)

type VirtualBoxHostAgent struct{}

func (vbha *VirtualBoxHostAgent) GetMachines(ctx context.Context, req *proto.GetMachinesRequest, rsp *proto.GetMachinesResponse) error {
	rsp.VM = vbox.GetMachines()
	return nil
}

func startAgentLoop(ctx context.Context, elog Logger) error {
	service := micro.NewService(
		micro.Name("virtualboxhostagent"),
		micro.RegisterTTL(time.Second*30),
		micro.RegisterInterval(time.Second*15),
		micro.Registry(
			consul.NewRegistry(consul.TCPCheck(time.Second*15)),
		),
	)

	service.Init()

	hostname := viper.GetString("Hostname")
	fmt.Printf("My hostname: %s\n", hostname)
	var err error
	vbox, err = NewLocalVirtualBox(hostname)
	if err != nil {
		panic(err)
	}

	go func() {
		vbox.LoadEnvironment()
		timeTick := time.Tick(time.Second * 5)
		for {
			select {
			case <-timeTick:
				vbox.LoadEnvironment()
			}
		}
	}()

	for {
		proto.RegisterVirtualBoxHostAgentHandler(service.Server(), new(VirtualBoxHostAgent))
		if err := service.Run(); err != nil {
			fmt.Println(err)
			time.Sleep(time.Second * 15)
		}
	}

	return nil
}
